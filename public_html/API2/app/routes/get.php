<?php
//consulta del catalogo de las entidades
$app->get('/entidades', function() use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'entidades' => array(
    )
  );
  $entidades = $app->db->query("select * from pib_scn.entidad");

if(!$entidades){
 $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de las entidades.");
}

  foreach ($entidades as $entidad) {
    $datos['entidades'][] = $entidad;
  }
  $app->response->setStatus(200);
  $app->render('get.php', $datos);
});





//consulta de una entidad por su clave(id)

$app->get('/entidades/:id', function($id) use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'entidades' => array(
    )
  );
  $entidades = $app->db->query("select * from pib_scn.entidad where clave=$id");
	
if(!$entidades){
 $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de las entidades.");
}

  foreach ($entidades as $entidad) {
    $datos['entidades'][] = $entidad;
  }
  $app->response->setStatus(200);
  $app->render('get.php', $datos);

});


//consulta de municipos

$app->get('/municipios', function() use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'municipios' => array(
    )
  );
  $municipios = $app->db->query("select * from pib_scn.municipio");

if(!$municipios){
 $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de los municipios.");
}

  foreach ($municipios as $municipio) {
    $datos['municipios'][] = $municipio;
  }
  $app->response->setStatus(200);
  $app->render('get.php', $datos);
});


//consulta de municipos por su id


$app->get('/municipios/:id', function($id) use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'municipios' => array(
    )
  );
  $municipios = $app->db->query("select * from pib_scn.municipio where clave=$id");
	
if(!$municipios){
 $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de los municipios.");
}

  foreach ($municipios as $municipio) {
    $datos['municipios'][] = $municipio;
  }
  $app->response->setStatus(200);
  $app->render('get.php', $datos);

});

//obtener todos lo indicadores

$app->get('/indicadores', function() use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'indicadores' => array(
    )
  );
  $indicadores = $app->db->query("select * from pib_scn.indicador");

if(!$indicadores){
 $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de los indicadores.");
}

  foreach ($indicadores as $indicador) {
    $datos['indicadores'][] = $indicador;
  }
  $app->response->setStatus(200);
  $app->render('get.php', $datos);
});

//obtener el indicador por su id

$app->get('/indicadores/:id', function($id) use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'indicadores' => array(
    )
  );
  $indicadores = $app->db->query("select * from pib_scn.indicador where clave=$id");

if(!$indicadores){
 $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de los indicadores.");
}

  foreach ($indicadores as $indicador) {
    $datos['indicadores'][] = $indicador;
  }
  $app->response->setStatus(200);
  $app->render('get.php', $datos);
});


//obtener toods los temas nivel 1

$app->get('/temas-nivel-1', function() use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'temas' => array(
    )
  );
  $temas = $app->db->query("select * from pib_scn.tema_nivel1");
	
if(!$temas){
 $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de los temas.");
}

  foreach ($temas as $tema) {
    $datos['temas'][] = $tema;
  }
  $app->response->setStatus(200);
  $app->render('get.php', $datos);

});


//obtener el tema nivel 1 por su id

$app->get('/temas-nivel-1/:id', function($id) use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'temas' => array(
    )
  );
  $temas = $app->db->query("select * from pib_scn.tema_nivel1 where clave=$id");
	
if(!$temas){
 $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de los temas.");
}

  foreach ($temas as $tema) {
    $datos['temas'][] = $tema;
  }
  $app->response->setStatus(200);
  $app->render('get.php', $datos);

});


//obtener toods los temas nivel 2

$app->get('/temas-nivel-2', function() use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'temas' => array(
    )
  );
  $temas = $app->db->query("select * from pib_scn.tema_nivel2");
	
if(!$temas){
 $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de los temas.");
}

  foreach ($temas as $tema) {
    $datos['temas'][] = $tema;
  }
  $app->response->setStatus(200);
  $app->render('get.php', $datos);

});


//obtener el tema nivel 2 por su id

$app->get('/temas-nivel-2/:id', function($id) use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'temas' => array(
    )
  );
  $temas = $app->db->query("select * from pib_scn.tema_nivel2 where clave=$id");
	
if(!$temas){
 $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de los temas.");
}

  foreach ($temas as $tema) {
    $datos['temas'][] = $tema;
  }
  $app->response->setStatus(200);
  $app->render('get.php', $datos);

});


//obtener toods los temas nivel 3

$app->get('/temas-nivel-3', function() use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'temas' => array(
    )
  );
  $temas = $app->db->query("select * from pib_scn.tema_nivel3");
	
if(!$temas){
 $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de los temas.");
}

  foreach ($temas as $tema) {
    $datos['temas'][] = $tema;
  }
  $app->response->setStatus(200);
  $app->render('get.php', $datos);

});


//obtener el tema nivel 3 por su id

$app->get('/temas-nivel-3/:id', function($id) use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'temas' => array(
    )
  );
  $temas = $app->db->query("select * from pib_scn.tema_nivel3 where clave=$id");
	
if(!$temas){
 $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de los temas.");
}

  foreach ($temas as $tema) {
    $datos['temas'][] = $tema;
  }
  $app->response->setStatus(200);
  $app->render('get.php', $datos);

});


//obtener todos los indicadores-monto

$app->get('/indicadores-monto', function() use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'montos' => array(
    )
  );
  $montos = $app->db->query("select * from pib_scn.indicador_monto");
	
if(!$montos){
 $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de los temas.");
}

  foreach ($montos as $monto) {
    $datos['montos'][] = $monto;
  }
  $app->response->setStatus(200);
  $app->render('get.php', $datos);

});

//obtener los indicadores por clave 

$app->get('/indicadores-monto/clave/:clave', function($clave) use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'montos' => array(
    )
  );
  $montos = $app->db->query("select * from pib_scn.indicador_monto where clave = $clave");
	
if(!$montos){
 $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de los temas.");
}

  foreach ($montos as $monto) {
    $datos['montos'][] = $monto;
  }
  $app->response->setStatus(200);
  $app->render('get.php', $datos);

});

//obtener todos los indicadores-monto por clave de identidad

$app->get('/indicadores-monto/entidades/:entidad', function($entidad) use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'montos' => array(
    )
  );
  $montos = $app->db->query("select * from pib_scn.indicador_monto where clave_entidad=$entidad");
	
if(!$montos){
 $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de los temas.");
}

  foreach ($montos as $monto) {
    $datos['montos'][] = $monto;
  }
  $app->response->setStatus(200);
  $app->render('get.php', $datos);

});




//obtener todos los indicadores-monto por clave de municipio

$app->get('/indicadores-monto/municipios/:municipio', function($municipio) use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'montos' => array(
    )
  );
  $montos = $app->db->query("select * from pib_scn.indicador_monto where clave_municipio=$municipio");
	
if(!$montos){
 $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de los temas.");
}

  foreach ($montos as $monto) {
    $datos['montos'][] = $monto;
  }
  $app->response->setStatus(200);
  $app->render('get.php', $datos);

});



//obtener todos los indicadores-monto por clave de tema-nivel-1

$app->get('/indicadores-monto/clave-tema-nivel-1/:municipio', function($tema) use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'montos' => array(
    )
  );
  $montos = $app->db->query("select * from pib_scn.indicador_monto where clave_tema_nivel1=$tema");
	
if(!$montos){
 $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de los temas.");
}

  foreach ($montos as $monto) {
    $datos['montos'][] = $monto;
  }
  $app->response->setStatus(200);
  $app->render('get.php', $datos);

});

//obtener todos los indicadores-monto por clave de tema-nivel-2

$app->get('/indicadores-monto/clave-tema-nivel-2/:municipio', function($tema) use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'montos' => array(
    )
  );
  $montos = $app->db->query("select * from pib_scn.indicador_monto where clave_tema_nivel2=$tema");
	
if(!$montos){
 $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de los temas.");
}

  foreach ($montos as $monto) {
    $datos['montos'][] = $monto;
  }
  $app->response->setStatus(200);
  $app->render('get.php', $datos);

});

//obtener todos los indicadores-monto por clave de tema-nivel-3

$app->get('/indicadores-monto/clave-tema-nivel-3/:municipio', function($tema) use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'montos' => array(
    )
  );
  $montos = $app->db->query("select * from pib_scn.indicador_monto where clave_tema_nivel3=$tema");
	
if(!$montos){
 $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de los temas.");
}

  foreach ($montos as $monto) {
    $datos['montos'][] = $monto;
  }
  $app->response->setStatus(200);
  $app->render('get.php', $datos);

});


//obtener todos los indicadores-monto por anio
$app->get('/indicadores-monto/anio/:anio', function($anio) use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'montos' => array(
    )
  );
  $montos = $app->db->query("select * from pib_scn.indicador_monto where anio=$anio");
	
if(!$montos){
 $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de los temas.");
}

  foreach ($montos as $monto) {
    $datos['montos'][] = $monto;
  }
  $app->response->setStatus(200);
  $app->render('get.php', $datos);

});








