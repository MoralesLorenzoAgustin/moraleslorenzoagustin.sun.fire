<?php
//eliminar una entidad por su clave
$app->delete('/entidades/:id', function($id) use($app) {
  

  /* EliminaciÃ³n en la bd */
  $qry = $app->db->prepare("delete from pib_scn.entidad where clave =$id");
  

  if ($qry->execute() === false) {
   
    $app->halt(500, "Error: No se ha podido borrar la entidad con id '$id'.");
  }

  if ($qry->rowCount() != 1) {
   
    $app->halt(404, "Error: La entidad con id '$id' no existe.");
  }

  $app->halt(200, "Exito: La entidad con id '$id' ha sido borrada");
});


//eliminar un municipio por su clave
$app->delete('/municipios/:id', function($id) use($app) {
  

  /* EliminaciÃ³n en la bd */
  $qry = $app->db->prepare("delete from pib_scn.municipio where clave = (:id_municipio)");
  $qry->bindParam(':id_municipio', $id);

  if ($qry->execute() === false) {
   
    $app->halt(500, "Error: No se ha podido borrar el municipio con id '$id'.");
  }

  if ($qry->rowCount() != 1) {
   
    $app->halt(404, "Error:El municipio con id '$id' no existe.");
  }

  $app->halt(200, "Exito: El municipio con id '$id' ha sido borrada");
});




//eliminar un indicador por su clave
$app->delete('/indicadores/:id', function($id) use($app) {
  

  /* EliminaciÃ³n en la bd */
  $qry = $app->db->prepare("delete from pib_scn.indicador where clave = (:id_indicador)");
  $qry->bindParam(':id_indicador', $id);

  if ($qry->execute() === false) {
   
    $app->halt(500, "Error: No se ha podido borrar el indicador con id '$id'.");
  }

  if ($qry->rowCount() != 1) {
   
    $app->halt(404, "Error:El indicador con id '$id' no existe.");
  }

  $app->halt(200, "Exito: El indicador con id '$id' ha sido borrada");
});



//eliminar un tema-nivel-1 por su clave
$app->delete('/temas-nivel-1/:id', function($id) use($app) {
  

  /* EliminaciÃ³n en la bd */
  $qry = $app->db->prepare("delete from pib_scn.tema_nivel1 where clave = (:id_tema1");
  $qry->bindParam(':id_tema1', $id);

  if ($qry->execute() === false) {
   
    $app->halt(500, "Error: No se ha podido borrar el tema nivel1 con id '$id'.");
  }

  if ($qry->rowCount() != 1) {
   
    $app->halt(404, "Error: El tema nivel1 con id '$id' no existe.");
  }

  $app->halt(200, "Exito: El tema nivel1 con id '$id' ha sido borrada");
});






//eliminar un tema-nivel-2 por su clave
$app->delete('/temas-nivel-2/:id', function($id) use($app) {
  

  /* EliminaciÃ³n en la bd */
  $qry = $app->db->prepare("delete from pib_scn.tema_nivel2 where clave = (:id_tema2");
  $qry->bindParam(':id_tema2', $id);

  if ($qry->execute() === false) {
   
    $app->halt(500, "Error: No se ha podido borrar el tema nivel2 con id '$id'.");
  }

  if ($qry->rowCount() != 1) {
   
    $app->halt(404, "Error: El tema nivel2 con id '$id' no existe.");
  }

 
  $app->halt(200, "Exito: El tema nivel2 con id '$id' ha sido borrada");
});



//eliminar un tema-nivel-3 por su clave
$app->delete('/temas-nivel-3/:id', function($id) use($app) {
  

  /* EliminaciÃ³n en la bd */
  $qry = $app->db->prepare("delete from pib_scn.tema_nivel3 where clave = (:id_tema3");
  $qry->bindParam(':id_tema3', $id);

  if ($qry->execute() === false) {
   
    $app->halt(500, "Error: No se ha podido borrar el tema nivel3 con id '$id'.");
  }

  if ($qry->rowCount() != 1) {
   
    $app->halt(404, "Error: El tema nivel3 con id '$id' no existe.");
  }

 
  $app->halt(200, "Exito: El tema nivel3 con id '$id' ha sido borrada");
});





